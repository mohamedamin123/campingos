
import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AdminlayoutComponent } from './layouts/adminlayout/adminlayout.component';
import { UserlayoutComponent } from './layouts/userlayout/userlayout.component';

const routes: Routes = [
  {path:'',component:UserlayoutComponent,children:[
    {path:'',loadChildren:()=>import('./views/user/home/home.module').then(m=>m.HomeModule)},
    {path:'login',loadChildren:()=>import('./views/user/signin/signin.module').then(m=>m.SigninModule)},
    {path:'register',loadChildren:()=>import('./views/user/signup/signup.module').then(m=>m.SignupModule)},
    {path:'profil',loadChildren:()=>import('./views/user/profil/profil.module').then(m=>m.ProfilModule)},
    {path:'shop',loadChildren:()=>import('./views/user/shop/shop.module').then(m=>m.ShopModule)},
    {path:'detail',loadChildren:()=>import('./views/user/detaill-shop/detaill-shop.module').then(m=>m.DetaillShopModule)},
    {path:'events',loadChildren:()=>import('./views/user/events/events.module').then(m=>m.EventsModule)},
  
    {path:'',redirectTo:'', pathMatch: 'full'} 
  ]},
  {path:'admin',component:AdminlayoutComponent,children:[
    {path:'dashboard',loadChildren:()=>import('./views/admin/dashboard/dashboard.module').then(m=>m.DashboardModule)},
    {path:'allusers',loadChildren:()=>import('./views/admin/all-utilisateurs/all-utilisateurs.module').then(m=>m.AllUtilisateursModule)},
    {path:'adminlogin',loadChildren:()=>import('./views/admin/adminlogin/adminlogin.module').then(m=>m.AdminloginModule)}
  ]},
  
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
