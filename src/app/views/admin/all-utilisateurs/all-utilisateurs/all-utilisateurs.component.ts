import { Component, OnInit } from '@angular/core';
import { utilisateur } from 'src/app/models/utilisateur.model';
import { UserCRUDService } from 'src/app/service/user-crud.service';
import { ActivatedRoute, Router } from '@angular/router';
@Component({
  selector: 'app-all-utilisateurs',
  templateUrl: './all-utilisateurs.component.html',
  styleUrls: ['./all-utilisateurs.component.css']
})
export class AllUtilisateursComponent implements OnInit {

 
  utilisateur?: utilisateur[];
  currentuser: utilisateur = {};
  currentIndex = -1;
  title = '';


  constructor(private UserCRUDService: UserCRUDService ) { }

  ngOnInit(): void {
    this.retrieveTutorials();

  }

  retrieveTutorials(): void {
    this.UserCRUDService.getAll()
      .subscribe(
        data => {
          this.utilisateur = data;
          console.log(data);
        },
        error => {
          console.log(error);
        });
  }

  refreshList(): void {
    this.retrieveTutorials();
    this.currentuser = {};
    this.currentIndex = -1;
  }

  setActiveTutorial(utilisateur: utilisateur, index: number): void {
    this.currentuser = utilisateur;
    this.currentIndex = index;
  }
  deleteTutorial(): void {
    this.UserCRUDService.delete(this.currentuser.id)
      .subscribe(
        response => {
          console.log(response);
         
        },
        error => {
          console.log(error);
        });
  }
}
